FROM rust:1-slim-bullseye
RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends \
    dbus-x11 python3-dbusmock gettext lua5.3 lua-zlib \
    appstream rustc cargo python3-pip libssl-dev pkg-config llvm \
    libglib2.0-dev-bin libglib2.0-dev policykit-1 lcov libc6-dbg \
    libclang-dev clang valgrind llvm-11 git build-essential curl \
  && rm -rf /var/lib/apt/lists/* \
  && ln -sf /usr/bin/llvm-symbolizer-11 /usr/bin/llvm-symbolizer
RUN pip3 --no-cache-dir install meson ninja python-dbusmock==0.28.4
RUN rustup update nightly && rustup default nightly
RUN cargo +nightly install -Z sparse-registry grcov
RUN git clone https://gitlab.alpinelinux.org/alpine/abuild \
  && cd abuild \
  && git checkout v3.5.0 \
  && sed 's|bin/ash|bin/bash|g' -i ./*.in && make install \
  && cd .. \
  && rm -rf abuild
ENV APK_TOOLS_VER=v2.14.4
RUN curl -O https://gitlab.alpinelinux.org/alpine/apk-tools/-/archive/$APK_TOOLS_VER/apk-tools-$APK_TOOLS_VER.tar.gz \
  && tar xf apk-tools-$APK_TOOLS_VER.tar.gz \
  && cd apk-tools-$APK_TOOLS_VER \
  && sed "s|doc/||" -i Makefile \
  && make LUA=no LIBDIR=/usr/lib/x86_64-linux-gnu/ install \
  && cd .. \
  && rm -rf ./apk-tools-v$APK_TOOLS_VER*
# These are needed for testing
RUN curl -O https://gitlab.com/pabloyoyoista/alpine-appstream-downloader/-/raw/main/alpine-appstream-downloader \
       && chmod 755 alpine-appstream-downloader \
       && mv alpine-appstream-downloader /usr/sbin/
