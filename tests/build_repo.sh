#!/usr/bin/env bash

set -e

$APK --allow-untrusted -X "$2"/repo add --root "$1" --initdb
export ABUILD_USERDIR="$2/abuildUserDir"
abuild-keygen -anq

mkdir -p "$2"

cp -r $(dirname "$0")/repo "$2"/abuilds

cd "$2"

for x in abuilds/*/APKBUILD; do
    case "$x" in
        # Don't build this, we build it later to test upgrades
        *test-a-new*|*test-c*) ;;
        *)
            echo "$x"
            pushd ${x%/*}
            APK="$APK --allow-untrusted --root $1" SUDO_APK="abuild-apk --root $1" REPODEST="$2" abuild -F clean unpack prepare build rootpkg update_abuildrepo_index
            popd
            ;;
    esac
done
