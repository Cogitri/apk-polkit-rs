use enumflags2::bitflags;

#[bitflags]
#[repr(u32)]
#[derive(Copy, Clone, Debug, PartialEq, Eq, zvariant::Type)]
pub enum DetailsFlags {
    Description,
    InstalledSize,
    Version,
    License,
    PackageState,
    Size,
    StagingVersion,
    Url,
}

#[cfg(test)]
mod test {
    use super::DetailsFlags;
    use zvariant::Type;

    #[test]
    fn signature() {
        assert_eq!(DetailsFlags::SIGNATURE, "u");
    }
}
