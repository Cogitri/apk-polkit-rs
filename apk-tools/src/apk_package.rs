/*
    Copyright: 2020 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

use crate::error::Error;
use apk_tools_sys::apk_package;
use std::convert::TryFrom;
use std::ffi::CStr;
use std::fmt;

/**
* The state of a package
*/
#[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Eq)]
pub enum PackageState {
    Available,
    Installed,
    PendingInstall,
    PendingRemoval,
    Upgradable,
    Downgradable,
    Reinstall,
}

/**
* An `ApkPackage` is a safe wrapper around an `apk_package`. Since the `apk_package` only lives
* for as long as the underlying `ApkDatabase` lives, its lifetime is rather restricted.
*/
#[derive(Debug)]
pub struct ApkPackage<'a> {
    apk_package: &'a apk_package,
    staging_version: Option<String>,
    package_state: PackageState,
}

impl<'a> ApkPackage<'a> {
    pub fn new(
        package: &'a apk_package,
        staging_package: Option<&apk_package>,
        package_state: PackageState,
    ) -> ApkPackage<'a> {
        assert!(!package.name.is_null());
        assert!(!package.version.is_null());
        ApkPackage {
            apk_package: package,
            staging_version: staging_package.and_then(|p| match unsafe { p.version.as_ref() } {
                Some(v) => String::try_from(v).ok(),
                None => None,
            }),
            package_state,
        }
    }

    pub fn name(&self) -> &str {
        if let Some(name) = unsafe { self.apk_package.name.as_ref() } {
            unsafe { CStr::from_ptr((name).name).to_str().unwrap() }
        } else {
            ""
        }
    }

    pub fn staging_version(&self) -> Option<&str> {
        self.staging_version.as_deref()
    }

    pub fn version(&self) -> String {
        String::try_from(unsafe { self.apk_package.version.as_ref().unwrap() }).unwrap()
    }

    pub fn package_state(&self) -> PackageState {
        self.package_state
    }

    pub fn license(&self) -> Option<String> {
        if let Some(license) = unsafe { self.apk_package.license.as_ref() } {
            return String::try_from(license).ok();
        }
        None
    }

    pub fn url(&self) -> Option<&str> {
        if self.apk_package.url.is_null() {
            None
        } else {
            unsafe { Some(CStr::from_ptr(self.apk_package.url).to_str().unwrap()) }
        }
    }

    pub fn description(&self) -> Option<&str> {
        if self.apk_package.description.is_null() {
            None
        } else {
            unsafe {
                Some(
                    CStr::from_ptr(self.apk_package.description)
                        .to_str()
                        .unwrap(),
                )
            }
        }
    }

    pub fn size(&self) -> usize {
        self.apk_package.size
    }

    pub fn installed_size(&self) -> usize {
        self.apk_package.installed_size
    }
}

impl fmt::Display for ApkPackage<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}-{}", self.name(), self.version())
    }
}

/**
* An `ApkPackageError` is a way to identify an error with a specific ApkPackage.
 */
// Should probably implement format, and with it Debug
#[derive(Debug)]
pub struct ApkPackageError {
    name: String,
    error: Error,
}

impl ApkPackageError {
    pub fn new(name: String, error: Error) -> ApkPackageError {
        ApkPackageError { name, error }
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn error_str(&self) -> String {
        self.error.to_string()
    }
}
