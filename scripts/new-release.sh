#!/bin/bash

set -e

current=$(grep -Po "version: '\K([0-9]*\.[0-9]*.[0-9]+)(?=')" meson.build)
major=$(cut -d '.' -f1 <<< "$current")
minor=$(cut -d '.' -f2 <<< "$current")
patch=$(cut -d '.' -f3 <<< "$current")

case $1 in
    major)
        next=$(echo $((major + 1)).0.0)
        ;;
    minor)
        next=$(echo $major.$((minor + 1)).0)
        ;;
    patch)
        next=$(echo $major.$minor.$((patch + 1)))
        ;;
    *)
        echo "Don't know what to do, exiting!"
        exit 1
    ;;
esac

sed -i "s/version: '$current'/version: '$next'/" meson.build
sed -i "s/version = \"$current\"/version = \"$next\"/" apk-polkit-rs/Cargo.toml
cargo update -p apk-polkit-rs


git commit -av
git tag v$next

meson dist -C build --no-tests
