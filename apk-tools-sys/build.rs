/*
    Copyright: 2020 Rasmus Thomsen <oss@cogitri.dev>
    SPDX-License-Identifier: GPL-3.0-or-later
*/

use std::env;
use std::path::PathBuf;

fn generate_bindings(include_paths: &Vec<&PathBuf>) {
    let mut builder = bindgen::Builder::default()
        .header("src/wrapper.h")
        .derive_copy(false)
        .derive_default(true)
        .size_t_is_usize(true)
        .formatter(bindgen::Formatter::None)
        .impl_debug(true)
        .blocklist_type("max_align_t")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()));

    for path in include_paths {
        builder = builder.clang_arg(format!("-I{}", path.to_str().unwrap()));
    }

    let bindings = builder.generate().expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");

    println!("cargo:rerun-if-changed=src/wrapper.h");
}

fn main() {
    let config = system_deps::Config::new()
        .probe()
        .expect("Couldn't find required system libraries");

    generate_bindings(&config.all_include_paths());
}
